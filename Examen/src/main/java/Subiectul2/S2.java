package Subiectul2;

import javax.swing.*;

public class S2 {
    public static void main(String[] args) {
        new Window2();
    }
}

class Window2 extends JFrame {

    public Window2() {
        this.setLayout(null);
        this.setSize(300, 300);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Copy");

        JTextArea zone1 = new JTextArea();
        zone1.setBounds(50, 50, 150, 20);
        JTextField field1 = new JTextField();
        field1.setBounds(50, 50, 150, 20);
        JTextField field2 = new JTextField();
        field1.setBounds(50, 50, 150, 20);


        JButton button = new JButton("Calculate!");
        button.setBounds(80, 150, 100, 20);
        button.addActionListener(e -> {
            int a, b, c;

            a = Integer.parseInt(field1.getText());
            b = Integer.parseInt(field2.getText());
            c = a + b;


            zone1.append(String.valueOf(c));


            this.add(zone1);
            this.add(button);
            this.setVisible(true);
        });
    }
}


